package com.here.android.tutorial;

import android.os.Parcel;
import android.os.Parcelable;

import com.here.android.mpa.common.GeoCoordinate;

public class Place implements Parcelable {
    private String mName;
    private String mDescription;
    private Double mLat;
    private Double mLong;

    public Place(String mName, String mDescription, GeoCoordinate geoCoordinate) {
        this.mName = mName;
        this.mDescription = mDescription;
        this.mLat = geoCoordinate.getLatitude();
        this.mLong = geoCoordinate.getLongitude();
    }

    public String getmName() {
        return mName;
    }

    public String getmDescription() {
        return mDescription;
    }

    public Double getmLat() {
        return mLat;
    }

    public Double getmLong() {
        return mLong;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(mName);
        dest.writeString(mDescription);
        dest.writeDouble(mLat);
        dest.writeDouble(mLong);
    }

    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>()
    {
        @Override
        public Place createFromParcel(Parcel source)
        {
            return new Place(source);
        }

        @Override
        public Place[] newArray(int size)
        {
            return new Place[size];
        }
    };

    public Place(Parcel in) {
        this.mName = in.readString();
        this.mDescription = in.readString();
        this.mLat = in.readDouble();
        this.mLong = in.readDouble();
    }

}
