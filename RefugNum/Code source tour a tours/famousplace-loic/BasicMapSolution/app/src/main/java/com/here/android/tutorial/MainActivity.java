package com.here.android.tutorial;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.here.android.mpa.common.GeoCoordinate;

public class MainActivity extends Activity {
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
    }

    public void onClickButton(View view) {
        Place place;

        Intent intent = new Intent(mContext, BasicMapActivity.class);
        switch (view.getId()) {
            case R.id.button1:
                Log.d("LOL", "bouton 1");
                place = new Place("Hôtel de ville", "L'édifice monumental, démesurément grand par rapport à la place Jean-Jaurès et au palais de Justice, est destiné à refléter les vertus républicaines et l'autorité municipale. L'hôtel de ville et la place Jean Jaurès évoquent un aménagement parisien.", new GeoCoordinate(47.3908716,0.6871433));

                break;
            case R.id.button2:
                Log.d("LOL", "bouton 2");

                place = new Place("Place des Amoureux", "La fontaine de Beaune-Semblançay est installée au centre du jardin du même nom, dans la partie nord de Tours", new GeoCoordinate(47.3939273,0.686474));
                break;
            case R.id.button3:
                Log.d("LOL", "bouton 3");

                place = new Place("Cathédrale", "", new GeoCoordinate(47.3956058, 0.6940343));
                break;
            default:
                Log.d("LOL", "defaut");

                place = new Place("New-York", "Quelque chose s'est ma passé", new GeoCoordinate(40.716709, -74.005698));
                break;

        }
        intent.putExtra(Util.LIEU, (Parcelable) place);
        intent.setPackage("com.here.app.maps");
        startActivity(intent);
    }
}
