/*
 * Copyright (c) 2011-2018 HERE Global B.V. and its affiliate(s).
 * All rights reserved.
 * The use of this software is conditional upon having a separate agreement
 * with a HERE company for the use or utilization of this software. In the
 * absence of such agreement, the use of the software is not allowed.
 */
package com.here.android.tutorial;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MenuItem;
import android.widget.Toast;
import android.util.Log;
import android.view.Menu;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.GeoPosition;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;

public class BasicMapActivity extends Activity {
    private static final String LOG_TAG = BasicMapActivity.class.getSimpleName();

    // permissions request code
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private Place mPlace;
    private GeoCoordinate geoCoordinate;
    private PositioningManager positioningManager;


    /**
     * Permissions that need to be explicitly requested from end user.
     */
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    // map embedded in the map fragment
    private Map map = null;

    // map fragment embedded in this activity
    private MapFragment mapFragment = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPermissions();

    }

    // Google has deprecated android.app.Fragment class. It is used in current SDK implementation.
    // Will be fixed in future SDK version.
    @SuppressWarnings("deprecation")
    private MapFragment getMapFragment() {
        return (MapFragment) getFragmentManager().findFragmentById(R.id.mapfragment);
    }

    private void initialize() {
        setContentView(R.layout.activity_basicmap);
        mPlace = (Place) getIntent().getParcelableExtra(Util.LIEU);

        // Search for the map fragment to finish setup by calling init().
        mapFragment = getMapFragment();
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    // retrieve a reference of the map from the map fragment
                    map = mapFragment.getMap();
                    geoCoordinate = new GeoCoordinate(mPlace.getmLat(), mPlace.getmLong());
                    MapMarker mapMarker = new MapMarker();


                    map.setZoomLevel(17);
                    map.setCenter(geoCoordinate, Map.Animation.NONE);
                    mapMarker.setCoordinate(geoCoordinate);
                    mapMarker.setTitle(mPlace.getmName());
                    Log.d("LOL", mPlace.getmName());
                    mapMarker.setDescription(mPlace.getmDescription());

                    map.addMapObject(mapMarker);
                    mapMarker.showInfoBubble();


// Create a gesture listener and add it to the MapFragment
                    MapGesture.OnGestureListener listener = new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                        @Override
                        public boolean onMapObjectsSelected(List<ViewObject> objects) {
                            for (ViewObject viewObj : objects) {
                                if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                                    if (((MapObject) viewObj).getType() == MapObject.Type.MARKER) {
                                        // At this point we have the originally added
                                        // map marker, so we can do something with it
                                        // (like change the visibility, or more
                                        // marker-specific actions)
                                        ((MapObject) viewObj).setVisible(false);
                                        Log.d("LOL", "test");
                                    }
                                }
                            }
                            // return false to allow the map to handle this callback also
                            return false;
                        }
                    };


                } else {
                    Log.e(LOG_TAG, "Cannot initialize MapFragment (" + error + ")");
                }
            }
        });
    }

    /**
     * Checks the dynamically controlled permissions and requests missing permissions from end user.
     */
    protected void checkPermissions() {
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                // all permissions were granted
                initialize();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_direction) {
            // Declare the rm variable (the RouteManager)
            RouteManager rm = new RouteManager();
            // Create the RoutePlan and add two waypoints
            RoutePlan routePlan = new RoutePlan();
            MapMarker geoLocMapMarker = new MapMarker();
            /*
            positioningManager = PositioningManager.getInstance();
            positioningManager.addListener(new WeakReference<PositioningManager.OnPositionChangedListener>(positionListener));

            if(positioningManager.start(PositioningManager.LocationMethod.GPS)){

                Log.d("LOL", "OK");
                Log.d("LOL","lat :" + positioningManager.getPosition().getLatitudeAccuracy());
                Log.d("LOL", "long : " + positioningManager.getPosition().getLongitudeAccuracy());
            } else {
                Log.d("LOL", "KO");
            }
            */

            routePlan.addWaypoint(geoCoordinate);
            routePlan.addWaypoint(new GeoCoordinate(47.3633001, 0.6811984));

            map.setZoomLevel(17);
            map.setCenter(new GeoCoordinate(47.3633001, 0.6811984), Map.Animation.LINEAR);
            geoLocMapMarker.setCoordinate(new GeoCoordinate(47.3633001, 0.6811984));
            map.addMapObject(geoLocMapMarker);


            // Create the RouteOptions and set its transport mode & routing type
            RouteOptions routeOptions = new RouteOptions();
            routeOptions.setTransportMode(RouteOptions.TransportMode.PEDESTRIAN);
            routeOptions.setRouteType(RouteOptions.Type.FASTEST);

            routePlan.setRouteOptions(routeOptions);
            rm.calculateRoute(routePlan, new RouteListener());
            // map fragment has been successfully initialized,
// now the map is ready to be used

// Display position indicator
            //map.getPositionIndicator().setVisible(true);


            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private class RouteListener implements RouteManager.Listener {

        // Method defined in Listener
        public void onProgress(int percentage) {
            // Display a message indicating calculation progress
        }

        // Method defined in Listener
        public void onCalculateRouteFinished(RouteManager.Error error, List<RouteResult> routeResult) {
            // If the route was calculated successfully
            if (error == RouteManager.Error.NONE) {
                // Render the route on the map
                MapRoute mapRoute = new MapRoute(routeResult.get(0).getRoute());
                map.addMapObject(mapRoute);
            } else {
                // Display a message indicating route calculation failure
            }
        }
    }

    private PositioningManager.OnPositionChangedListener positionListener = new PositioningManager.OnPositionChangedListener() {

        public void onPositionUpdated(PositioningManager.LocationMethod method, GeoPosition position, boolean isMapMatched) {
            // set the center only when the app is in the foreground
            // to reduce CPU consumption
            //if (!paused) {
                map.setCenter(position.getCoordinate(), Map.Animation.NONE);
            //}
        }

        public void onPositionFixChanged(PositioningManager.LocationMethod method, PositioningManager.LocationStatus status) {
        }
    };
}
