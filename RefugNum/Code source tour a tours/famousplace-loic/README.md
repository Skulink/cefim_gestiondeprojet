Gestion de l'API de la Map. 
L'application s'appuie sur le SDK de HERE. A toutes fins utiles, le lien ci-dessous décrit la procédure pour démarrer l'application après téléchargement du SDK de HERE :
https://developer.here.com/documentation/android-premium/dev_guide/topics/app-run-simple.html
Fichiers situés dans un dossier BasicMapSolution. L'app est en fait une customisation de l'app exemple de HERE.

Package com.here.android.tutorial :
Classe MainActivity (layout activity_main):
Activité principale : menu d'entrée de l'app. Stockage en dur des lieux.

Classe BasicMapActivity (layout activity_basicmap):
Activité de cartographie. Les fonctionnalités de cartographie.
La géolocalisation est désactivée //positioningManager = PositioningManager.getInstance().
Géolocalisation en dur (Pas bien !!!).
 les infos et la géolocalisation des lieux sont passées via l'objet mPlace qui reçoit les données de l'activité principale moyennant l'utilisation de l'interface Parcelable.

Classe Place :
Definit les données sur les lieux.

Classe Util :
Définit la clé pour le passage des objets place entre l'activité menu et l'activité de cartographie.